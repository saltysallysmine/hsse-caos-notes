#include <stdio.h>

unsigned long long rdtsc(void) {
  unsigned long long tsc;
  asm volatile("rdtsc" : "=A"(tsc));
  return tsc;
}

int main() {
  unsigned long long start, end, ticks;

  start = rdtsc();
  // do the thing
  end = rdtsc();

  ticks = end - start;
  printf("Ticks: %llu\n", ticks);
}
