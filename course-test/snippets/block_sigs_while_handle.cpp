#include <signal.h>

void sig_handler(int signo) {
  // Блокируем все сигналы
  sigset_t mask;
  sigfillset(&mask);
  sigprocmask(SIG_BLOCK, &mask, NULL);

  // Обработка сигнала

  // Разблокируем все сигналы
  sigprocmask(SIG_UNBLOCK, &mask, NULL);
}
