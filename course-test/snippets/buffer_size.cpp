#include <time.h>

#include <iostream>

int main() {
  for (int i = 0; i < 2000; ++i) {
    struct timespec req;
    req.tv_nsec = 1'000'000;
    struct timespec rem;
    std::cout << nanosleep(&req, &rem);
  }
  return 0;
}
