#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 4096

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Использование: %s <исходный_файл> <целевой_файл>\n", argv[0]);
        return 1;
    }

    int source_fd = open(argv[1], O_RDONLY);
    if (source_fd == -1) {
        perror("Ошибка при открытии исходного файла");
        return 1;
    }

    int dest_fd = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (dest_fd == -1) {
        perror("Ошибка при открытии целевого файла");
        close(source_fd);
        return 1;
    }

    char buffer[BUFFER_SIZE];
    ssize_t bytes_read;
    while ((bytes_read = read(source_fd, buffer, BUFFER_SIZE)) > 0) {
        if (write(dest_fd, buffer, bytes_read) != bytes_read) {
            perror("Ошибка при записи в целевой файл");
            close(source_fd);
            close(dest_fd);
            return 1;
        }
    }

    if (bytes_read == -1) {
        perror("Ошибка при чтении из исходного файла");
        close(source_fd);
        close(dest_fd);
        return 1;
    }

    if (close(source_fd) == -1) {
        perror("Ошибка при закрытии исходного файла");
        return 1;
    }

    if (close(dest_fd) == -1) {
        perror("Ошибка при закрытии целевого файла");
        return 1;
    }

    return 0;
}
