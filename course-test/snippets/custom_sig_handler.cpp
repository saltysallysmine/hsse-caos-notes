#include <signal.h>
#include <stdio.h>
#include <unistd.h>

void sigsegv_handler(int signal) {
  printf("Caught signal %d\n", signal);
  sleep(10);
  printf("Wake up!\n");
}

int main() {
  signal(SIGINT, sigsegv_handler);   // делаем ^C
  signal(SIGQUIT, sigsegv_handler);  // делаем ^\ (ctrl-4)

  while (1) {
  }
}
