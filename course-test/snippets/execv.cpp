#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>

int main() {
  printf("Hello\n");

  int pid = fork();
  if (pid == 0) {
    char* argv[] = {"/usr/bin/ls", "/home/valery/Pictures", NULL};
    int res = execv("/usr/bin/ls", argv);
  }
  int x;
  pid = wait(&x);  // разбуди, когда один из детей закончится, верни его статус
  printf("x=%d", x);  // x = 0, если в ребёнке был return 0
  return 0;
}
