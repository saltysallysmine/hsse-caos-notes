#include <linux/futex.h>
#include <sys/syscall.h>
#include <unistd.h>

class FutexMutex {
 public:
  void lock() {
    int c = 0;
    if (__sync_bool_compare_and_swap(&m_flag, 0, 1)) {
      return;
    }

    syscall(SYS_futex, &m_flag, FUTEX_WAIT, 1, nullptr, nullptr, 0);
  }

  void unlock() {
    m_flag = 0;
    syscall(SYS_futex, &m_flag, FUTEX_WAKE, 1, nullptr, nullptr, 0);
  }

 private:
  int m_flag = 0;
};
