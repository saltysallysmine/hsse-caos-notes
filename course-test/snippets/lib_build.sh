#!/bin/bash

g++ -shared -o libmyfunc.so myfunc.cpp # сборка библиотеки
g++ main.cpp -lmyfunc -L. # сборка приложения
                          # именно так: без пробела и без lib в начале
LD_LIBRARY_PATH=custom/path && ./a.out # запуск
# g++ main.cpp -lmyfunc -L. -Wl,-rpath,/absolute/path
