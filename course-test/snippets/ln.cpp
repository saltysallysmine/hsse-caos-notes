#include <fcntl.h>
#include <unistd.h>

#include <iostream>

int main(int argc, char* argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " source destination" << std::endl;
    return 1;
  }

  int dir_fd = open(
      ".", O_DIRECTORY);  // получаем файловый дескриптор текущей директории

  if (dir_fd == -1) {
    std::perror("open");
    return 1;
  }

  if (linkat(AT_FDCWD, argv[1], dir_fd, argv[2], 0) ==
      -1) {  // передаем дескриптор исходной директории (AT_FDCWD - для
             // текущей), имя исходного файла, дескриптор текущей директории,
             // имя желаемого файла и флаги (0 - для базового поведения)
    std::perror("linkat");
    return 1;
  }

  close(dir_fd);
  return 0;
}
