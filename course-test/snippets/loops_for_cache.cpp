// Loop 1
for (int i = 0; i < arr.Length; i++) {
  arr[i] *= 3;
}

// Loop 2
for (int i = 0; i < arr.Length; i += 16) {
  arr[i] *= 3;
}
