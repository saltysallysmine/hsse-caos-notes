#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

int main() {
  int size = 4096;                          // размер в байтах
  int protection = PROT_READ | PROT_WRITE;  // разрешение на чтение и запись
  int visibility =
      MAP_SHARED | MAP_ANONYMOUS;  // общая память, не связанная с файлом

  void* addr = mmap(NULL, size, protection, visibility, -1, 0);
  if (addr == MAP_FAILED) {
    // Обработка ошибки
  }

  void* new_addr = mremap(addr, size, size * 2, MREMAP_MAYMOVE);
  if (new_addr == MAP_FAILED) {
    // Обработка ошибки
  } else {
    addr = new_addr;
  }

  munmap(addr, size * 2);  // освобождение памяти
  return 0;
}
