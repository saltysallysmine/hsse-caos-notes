#include <unistd.h>

#include <iostream>

int main(int argc, char* argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " source destination" << std::endl;
    return 1;
  }

  if (rename(argv[1], argv[2]) == -1) {
    std::perror("rename");
    return 1;
  }

  return 0;
}
