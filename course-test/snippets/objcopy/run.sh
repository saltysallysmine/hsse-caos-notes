#!/bin/bash

g++ -c func.cpp -o func.o
objcopy --keep-symbol=func func.o func_section.o
g++ main.cpp func_section.o -o main.out
./main.out
