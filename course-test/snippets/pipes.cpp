#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

int main(int argc, char* argv[]) {  // ./a.out somestring
  char buf[1000];
  int pipefd[2];  // [0] - read, [1] - write
  pipe(pipefd);

  int cpid = fork();
  // after fork child and parent have both ends!
  if (cpid == 0) {     // child reads from pipe
    close(pipefd[1]);  // close unused write end

    while (read(pipefd[0], &buf, 1) > 0) {
      write(STDOUT_FILENO, &buf, 1);
    }

    write(STDOUT_FILENO, "\n", 1);
    close(pipefd[0]);
    _exit(0);

  } else {             // parent writes argv[1] to pipe
    close(pipefd[0]);  // close unused read end
    write(pipefd[1], argv[1], strlen(argv[1]));
    close(pipefd[1]);  // reader will see EOF
    wait(NULL);        // wait for child
    exit(0);
  }
}
