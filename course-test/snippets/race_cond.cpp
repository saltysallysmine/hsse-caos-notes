#include <iostream>
#include <thread>

int cnt = 0;

void f() {
  for (int i = 0; i < 1'000'000; ++i) {
    cnt++;
  }
}

int main() {
  std::thread t1(&f);
  std::thread t2(&f);
  t1.join();
  t2.join();
  std::cout << cnt;
  return 0;
}
