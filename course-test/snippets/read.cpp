#include <cstdio>
#include <unistd.h>

int main() {
  char buff[100];
  int num = read(0, buff, 10);
  printf("%d", num); // у printf тоже есть свой буфер!
  write(1, buff, 5);
  // если тут позвать exit, то printf не вызовется совсем
}
