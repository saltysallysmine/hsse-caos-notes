#include <fcntl.h>
#include <unistd.h>

#include <iostream>

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " file1 [file2...]" << std::endl;
    return 1;
  }

  int dir_fd = open(
      ".", O_DIRECTORY);  // получаем файловый дескриптор текущей директории

  if (dir_fd == -1) {
    std::perror("open");
    return 1;
  }

  for (int i = 1; i < argc; i++) {
    if (unlinkat(dir_fd, argv[i], 0) ==
        -1) {  // передаем файловый дескриптор текущей директории, имя файла и
               // флаги (0 - для базового поведения)
      std::perror("unlinkat");
      return 1;
    }
  }

  close(dir_fd);
  return 0;
}
