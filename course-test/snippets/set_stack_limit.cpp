#include <stdio.h>
#include <sys/resource.h>

int main() {
  struct rlimit rlim;

  // Получаем текущий лимит размера стека
  if (getrlimit(RLIMIT_STACK, &rlim) == 0) {
    printf("Текущий лимит размера стека: soft=%ld, hard=%ld\n", rlim.rlim_cur,
           rlim.rlim_max);
  } else {
    perror("Ошибка при получении текущего лимита размера стека");
    return 1;
  }

  // Устанавливаем новый максимальный лимит размера стека
  rlim.rlim_cur = 16 * 1024 * 1024;  // Новый размер стека в байтах
  if (setrlimit(RLIMIT_STACK, &rlim) != 0) {
    perror("Ошибка при установке нового лимита размера стека");
    return 1;
  }

  // Повторно получаем лимит размера стека для проверки
  if (getrlimit(RLIMIT_STACK, &rlim) == 0) {
    printf("Новый лимит размера стека: soft=%ld, hard=%ld\n", rlim.rlim_cur,
           rlim.rlim_max);
  } else {
    perror("Ошибка при получении нового лимита размера стека");
    return 1;
  }

  return 0;
}
