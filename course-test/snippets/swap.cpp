#include <iostream>
using namespace std;

int main() {
  int a = 5;
  int b = 10;

  cout << "Before swap: a = " << a << ", b = " << b << endl;

  asm("xor %[a], %[b];"
      "xor %[b], %[a];"
      "xor %[a], %[b];"
      : [a] "+r"(a), [b] "+r"(b)
      :
      : "cc");

  cout << "After swap: a = " << a << ", b = " << b << endl;

  return 0;
}
