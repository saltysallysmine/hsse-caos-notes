#include <fcntl.h>
#include <unistd.h>

#include <iostream>

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " file1 [file2...]" << std::endl;
    return 1;
  }

  int fd;
  if ((fd = open(argv[1], O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1) {
    std::perror("open");
    return 1;
  }

  int orig_stdout = dup(1);  // Сохраняем стандартный вывод

  for (int i = 0; i < argc; i++) {
    if (i > 1) {
      int fd_out = open(argv[i], O_WRONLY | O_CREAT | O_TRUNC, 0644);
      if (fd_out == -1) {
        std::perror("open");
        return 1;
      }
      dup2(fd_out, 1);  // Переназначаем стандартный вывод на файл
    }

    char buffer[4096];
    ssize_t bytes_read;
    while ((bytes_read = read(0, buffer, sizeof(buffer))) > 0) {
      if (write(1, buffer, bytes_read) != bytes_read) {
        std::perror("write");
        return 1;
      }
      if (i > 1 && write(fd_out, buffer, bytes_read) != bytes_read) {
        std::perror("write");
        return 1;
      }
    }

    if (i > 1) {
      close(fd_out);
    }
  }

  dup2(orig_stdout, 1);  // Восстанавливаем стандартный вывод
  close(fd);

  return 0;
}
