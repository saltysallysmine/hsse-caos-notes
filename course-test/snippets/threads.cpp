#include <iostream>
#include <thread>

void f() {
  for (int i = 0; i < 1000; ++i) {
    std::cout << i << '\n';
  }
}

int main() {
  std::thread t(&f);  // можно также передать лямбду
  for (int i = 1000; i < 2000; ++i) {
    std::cout << i << '\n';
  }

  t.join();  // аналог wait for child
}
