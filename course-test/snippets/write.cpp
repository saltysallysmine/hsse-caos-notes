#include <unistd.h>

int main() {
  char buffer[] = "Hello world!";
  write(1, buffer, sizeof(buffer));
  _exit(0);
}
