#!/bin/bash

# make notes-by-date

for file in src/*; do
	if [[ -d $file ]] || [[ $file == "src/conf.typ" ]]; then
		continue
	fi
	file=${file#src/}
	typst compile src/$file notes-by-date/${file%.typ}.pdf
done

# make all-caos-notes

ALL_NOTES=src/all-caos-notes
if [[ -f $ALL_NOTES.typ ]]; then
	rm $ALL_NOTES.typ
	touch $ALL_NOTES.typ
fi

for file in src/*; do
	if [[ -d $file ]] || [[ $file == "src/conf.typ" ]] || [[ $file == "src/$ALL_NOTES.typ" ]]; then
		continue
	fi
	cat $file >>$ALL_NOTES.typ
done

typst compile $ALL_NOTES.typ all-caos-notes.pdf
rm $ALL_NOTES.typ

# make course-test
COURSE_TEST=src/course-test
typst compile $COURSE_TEST.typ course-test/course-test.pdf
