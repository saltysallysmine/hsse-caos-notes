#import "conf.typ"

#show: doc => conf.conf(
  [2023-09-13-АКОС],
  doc,
)

= Низкоуровневое-программирование

== Разбор упражнения на линковку

После запуска `a.out` запускается динамический линковщик. Затем запускается некая функция `_start`, после неё - `main`. Для динамического линковщика в решении нужен параметр `-I`, для того, чтобы линковщик не считал `main` начальной функцией  `_start` - параметр `-e`.

```bash
ld -e main a.o /usr/lib/libstdc++.so.6 /usr/lib/libm.so.6 /usr/lib/libgcc_s.so.1 /usr/lib/libc.so.6 /usr/lib64/ld-linux-x86-64.so.2 -I /lib64/ld-linux-x86-64.so.2
./a.out
> 5
seg. fault
```

Падение, потому что после исполнения инструкций программа пытается выйти из `main`, но у неё это не получается. На выходе из `_start` такого нет, потому что у него прописана специальная инструкция `exit`, говорящая ОС завершить работу программы.

#conf.delimiter

```bash
g++ -g main.cpp # скомпилировать программу с некоторым дополнительным кодом для отладки
```

Зачастую у проекта есть дебажная и релизная сборки. Первую можно дебажить, на ней ищут ошибки, а вторую выпускают в прод.

Дебажная версия по отношению к релизной весит довольно много. Для простой программы с вектором первая весит 10МБ, а вторая - 2МБ.

```bash
gbd a.out
(gdb) run
(gdb) b main # поставить точку останова на начало main
(gdb) run
> _libc_start_main # некая функция, запущенная до main
-------------------------------------------------------
(gdb) n # next line
(gdb) s # зайти в строчку
(gdb) l # показывает код вокруг строчкиЮ на которой мы остановились
(gdb) quit
(gdb) directory /path # указать путь до директории с либой
```

== Системные вызовы и ввод и вывод

Любая программа, взаимодействующая с внешним миром, вынуждена делать это через ОС, иначе не сделать. 

#image("img/interation-with-computer-layers.jpg")

*Системный вызов* - интерфейс, предоставляемый ОС программам, прослойка между ними и пользователем.

`strace` показывает, какие системные вызовы делает программа.

```bash
$ strace a.out
execve 'a.out' [...] # это запуск самой программы
...
exit_group(0)
+++ exited with 0 +++
```

Давайте завершим упражнение.

```cpp
#include <iostream>

int main() {
	std::cout << "Hello world!";
	exit(0);
}
```

- `exit` - библиотечная функция, делающая системный вызов. Чтобы обратиться напрямую к системе, нужно использовать `_exit`.

```cpp
#include <iostream>
#include <unistd.h>

int main() {
	std::cout << "Hello world!";
	_exit(0);
}
```

Почему не выводится строка, хотя нет fault? У `cout` есть свой буфер! А мы обрубаем завершение программы, не дожидаясь ничего.

```cpp
#include <iostream>
#include <unistd.h>

int main() {
	std::cout << "Hello world!";
	std::cout.flush(); // заставляет cout вывести буфер
	_exit(0);
}
```

#conf.delimiter

Теперь, используя `read` и `write`, будем работать без `iostream`.

```bash
man 2 write # посмотреть вторую главу man (системные вызовы), найти в ней man
```

```cpp
#include <unistd.h>

int main() {
	char buffer[] = "Hello world!";
	write(1, buffer, 10);
	_exit(0);
}
// Выведет Hello worl, потому что попросили 10 символов
```

```cpp
#include <unistd.h>

int main() {
	char buffer[] = "Hello world!";
	write(1, buffer, sizeof(buffer));
	_exit(0);
}
// Hello world! без \n
```

`write` может завершиться безуспешно и не вернуть управление нам. В _man_ есть целый список причин этого.

```cpp
#include <unistd.h>

int main() {
	char buffer[] = "Hello world!";
	for (int i = sizeof(buffer); i > 0; ) {
		i -= write(1, buffer + sizeof(buffer) - i, sizeof(buffer));
	}
	_exit(0);
}
```

Но write может вернуть -1!

```cpp
#include <unistd.h>
#include <errno.h> // там лежит код последней выпавшей ошибки

int main() {
	char buffer[] = "Hello world!";
	for (int i = sizeof(buffer); i > 0; ) {
		i -= write(1, buffer + sizeof(buffer) - i, sizeof(buffer));
	}
	_exit(0);
}
```

== Примечания

- `ltrace` - следить за тем, какие библиотечные вызовы делает программа.
- `ps` - показать процессы. Например, `ps -ef | grep firefox`.
- `g++ -no-pie test.cpp` - no position, independent, executable. Формат pie был придуман для защиты от хакерских атак. Однако для readelf такие файлы могут считаться динамическими (DYN).
- `g++ -static a.out` - скомпилировать статически. Размер файла будет большим.
- `man write`  - найти что-то с именем write. Что если есть системный вызов _write_ и программа _write_? Используем вторую главу _man_: `man 2 write`.
