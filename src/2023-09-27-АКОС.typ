#import "conf.typ"

#show: doc => conf.conf(
  [2023-09-27-АКОС],
  doc,
)

= Низкоуровневное-программирование

=== Write (вспомним прошлое занятие)

```cpp
#include <errno.h>
#include <unistd.h>

int main() {
  char buffer[] = "Hello world!";
  for (int i = sizeof(buffer); i > 0;) {
    i -= write(1, buffer + sizeof(buffer) - i, sizeof(buffer));
  }
  _exit(0);
}
```

#conf.delimiter

Простой пример обработки ошибок.

```cpp
#include <cstdio>
#include <errno.h>
#include <unistd.h>

int main() {
  char buffer[] = "Hello world!";
  char *ptr = (char *)0x900000;
  for (int i = sizeof(buffer); i > 0;) {
    ssize_t result = write(1, ptr, sizeof(buffer));
    if (result > 0) {
      i -= result;
      continue;
    }
    // result < 0
    if (errno == EFAULT) {
      printf("Efailt!");
      break;
    }
  }
}
```

- Когда вы вызываете `printf`, значение `errno` может поменяться! Поэтому плохо вызывать его в условии _(1)_.

=== Read

См. `man 2 read`.

```cpp
#include <cstdio>
#include <unistd.h>

int main() {
  char buff[100];
  int num = read(0, buff, 10);
  printf("%d", num); // у printf тоже есть свой буфер!
  write(1, buff, 5);
  // если тут позвать exit, то printf не вызовется совсем
}
```

После выполнения выведется `Hello10`, а в командной строке окажутся символы `d!`. Почему? Нажатие enter вызывает пуш данных в программу, но она не была готова съесть все 10 символов.

== Файлы

Будем учиться открывать файлы. Системный вызов `open` (`man 2 open`, там же можно посмотреть нужные библиотеки). Возвращает _id_, по которому в дальнейшем в ОС мы будем идентифицировать открытый файл.

*Файловый дескриптор* - то самое id, которое возвращает open, - число, с которым нами и ОС сопоставляется файл.

```cpp
#include <cstdio>
#include <fcntl.h>

int main() {
  int fd = open("/home/valery/Works/CAOS/2023-09-27/file.txt", O_RDONLY);
  printf("%d", fd); // выведет id, например, тройку
}
```

Что если читать из файла, который изменится или будет удалён другой программой? При удалении мы, скорее всего, будем ловить ошибки. При изменении, скорее всего, будет UB на уровне syscall'ов.

```cpp
#include <cstdio>
#include <fcntl.h>
#include <unistd.h>

int main() {
  int fd = open("/home/valery/Works/CAOS/2023-09-27/file.txt", O_RDONLY);
  char buff[1000];
  read(fd, buff, 10);
  write(1, buff, 10);
}
```

Будем открывать один файл на чтение и один на запись, создавая его, если он не создан.

```cpp
#include <cstdio>
#include <fcntl.h>
#include <unistd.h>

int main() {
  int fdread = open("/home/valery/Works/CAOS/2023-09-27/file.txt", O_RDONLY);
  int fdwrite = open("/home/valery/Works/CAOS/2023-09-27/hello.txt",
                     O_WRONLY | O_CREAT); // создать файл, если его не было
  char buff[1000];
  read(fdread, buff, 10);
  write(fdwrite, buff, 10); // у созданного файла нет прав на чтение
  close(fdread);
  close(fdwrite);
}
```

*Упражнение.* Теперь мы умеем открывать файлы на чтение и запись. Попробуйте реализовать команду копирования (`cp`). Второе упражнение: реализовать `tee` (от англ. _раздвоитель_): раздвоить вывод, направляя его в файл и _STDOUT_.

`dup` - дублицировать файловый дескриптор. `dup2` - присвой одному файловому дескриптору второй, теперь у нас есть два дескриптора одного файла.

```cpp
#include <fcntl.h>
#include <iostream>
#include <unistd.h>

int main() {
  int fd =
      open("/home/valery/Works/CAOS/2023-09-27/output.txt", O_WRONLY | O_CREAT);
  dup2(1, fd);
  std::cout << "blabla";
  std::cout.flush();
  close(fd);
}
```

`lseek` - reposition read/write file offset. Я читаю файл, где я сейчас стою, помнит система. lseek позволяет сразу встать на необходимое место в файле, чтобы не читать его с начала.

Осторожно! lseek позволяет встать дальше конца файла. Тогда в системе как бы создаётся дыра, запишутся нулевые байты. Если наивно реализовать `cp`, можно это не учесть и получить два одинаковых файла разных размеров.

- Право чтения отличается от права исполнения. Исполнение: загрузить в ОС бинарный код и выполнить его, как инструкции для процессора.

== Необычные файлы в Linux

*Директория.* (d - directory). В ней лежит список тех файлов, которые в нём лежат. Права директории интерпретируются иначе. У директории есть право на исполнение, что означает, что её можно открыть.

*Символическая ссылка.* (l - link). `ln -s a.out run` - создать файл, явл-ся ссылкой на a.out.

*Символьное устройство.* (c character).

*Блочное устройство.* (b block).

*Труба.* (p pipe). Посылать данные в другой процесс на компьютере.

*Сокет.* (s socket). Посылать данные по сети.

#conf.delimiter

Что такое ссылки на самом деле? Жёсткие ссылки: есть два разных имени, которые ссылаются на одно и то же место в памяти компьютера (`name` $->$ `segment of data` $<-$ `name2`). Символическая ссылка указывает не на объект на жёстком диске, а на имя файла (`name2` $->$ `name` $->$ `segment of data`).

Когда жёстких ссылок, ссылающихся на сегмент памяти нет, ОС считает, что файл был удалён. В этом случае обращение по символической ссылке приведёт к ошибке.

== Примечания

- `strace` - чтобы смотреть, сколько раз вызывался `write`.
- Длина буфера у `cout` - 1024.
- `chmod 777 file` - выдать все права на файл.
- `ls -l . -- > a.txt` - направить вывод в файл.
- `hd` - вывести файл бинарно.

Вывод `ls -la`:

```
total 52K
drwxr-xr-x 5 valery valery 4,0K сен 27 20:06 .
drwxr-xr-x 3 valery valery 4,0K сен 27 18:44 ..
-rwxr-xr-x 1 valery valery  16K сен 27 19:21 a.out
-rw-r--r-- 1 valery valery  294 сен 27 19:30 a.txt
drwxr-x--- 4 valery valery 4,0K сен 27 18:44 .ccls-cache
drwxr-xr-x 2 valery valery 4,0K сен 27 20:06 directory
-rw-r--r-- 1 valery valery   10 сен 27 19:17 file.txt
drwxr-xr-x 7 valery valery 4,0K сен 27 18:45 .git
-rw-r--r-- 1 valery valery   13 сен 27 18:46 .gitignore
---------- 1 valery valery    0 сен 27 19:21 hello.txt
-rw-r--r-- 1 valery valery  241 сен 27 19:39 main.cpp
```

Цифры во второй колонке означают количество ссылающихся на сегмент жёстких ссылок.
