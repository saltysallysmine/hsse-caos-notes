#import "conf.typ"

#show: doc => conf.conf(
  [2023-10-21-АКОС],
  doc,
)

= АКОС

=== Маппинг (продолжаем прошлую лекцию)

`mmap` работает лениво: если вы попросите подгрузить большой файл, он не будет сразу полностью его загружать.

Можно ли что-то поменять в файле? Да. Дадим себе право на запись и добавим данных. Однако нужно воспользоваься также `msync` - синхронизация файла в диске с тем, что замаплено в memory map.

```c
// Source: GPT
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

int main() {
    int fd;
    char *file_data;
    char *new_data = "New content";
    size_t file_size = strlen(new_data);

    // Открываем файл для чтения и записи
    fd = open("file.txt", O_RDWR);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    // Получаем указатель на начало файла
    file_data = mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (file_data == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    // Копируем новые данные в кэш
    memcpy(file_data, new_data, file_size);

    // Синхронизируем изменения в кэше с физическим файлом на диске
    if (msync(file_data, file_size, MS_SYNC) == -1) {
        perror("msync");
        exit(EXIT_FAILURE);
    }

    // Освобождаем память и закрываем файл
    munmap(file_data, file_size);
    close(fd);

    return 0;
}
```

#conf.delimiter

`readelf -a func.o`. В колонке Align лежит положение функции в нашем бинаре. В примере на лекции мы подгрузили инструкции из бинаря `func.o`, сдвинувшись на `0x<align>`. Примерный код приведён дальше.

```c
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

typedef double (*func_t)(double);

int main(int argc, char* argv[]) {
  const char* file_name = argv[1];
  double argument = strtod(argv[2], NULL);

  int fd = open(file_name, O_RDONLY);
  struct stat st = {};
  fstat(fd, &st);

  void* addr =
      mmap(NULL, st.st_size, PROT_READ | PROT_EXEC, MAP_PRIVATE, fd, 0);
  func_t func = (func)((char*)addr + 0x40);

  close(fd);
  double result = func(argument);
  printf("func(%f) = %f\n", argument, result);
  munmap(addr, st.st_size);
}
```

- `objcopy` - откусить часть объектного файла. Можно вырвать `func` из `func.o`.

#conf.note(
  text[
    `mmap` не единственный способ расширить своё адресное пространство. Есть ещё `brk` (break) - подвинуть границу _data-сегмента_. Используется в паре с `sbrk` - узнать текущую границу _data-сегмента_.
    `malloc` старается реже вызывать маппинг, потому что это дорогая операция! Маллок содержит в себе структуру, которую использует для маленьких выделений памяти, чтобы не создавать новую страницу на каждый вызов. На маленькие куски маллок просит brk.
  ]
)

== Процессы

Будем разбираться, как ОС управляет процессами.

- `top, htop`, `ps` (process show) -  команды, с помощью которых можно следить за процессами.
Процессы образуют дерево, потому что у каждого p есть тот, кто его запустил. Поэтому в выводе `ps -ef` мы увидим pid и ppid (parent pid).

Тяжёлые программы запускают много процессов для вкладок, подкачки видео и т.д.

- 0 - фейковый процесс.
- 1 - `/sbin/init splash` - родитель всех процессов.

```bash
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 14:24 ?        00:00:01 /sbin/init splash
root           2       0  0 14:24 ?        00:00:00 [kthreadd]
root           3       2  0 14:24 ?        00:00:00 [rcu_gp]
root           4       2  0 14:24 ?        00:00:00 [rcu_par_gp]
root           5       2  0 14:24 ?        00:00:00 [slub_flushwq]
```

Как программно запустить процесс? syscalls `fork` (вилка) & `exec`. fork создаёт полную копию рабочего процесса.

```c
#include <stdio.h>
#include <unistd.h>

int main() {
  printf("Hello\n");
  int pid = fork();
  if (pid == 0) {
    printf("I am child!\n");
  } else if (pid > 0) {
    printf("I am parent!\n");
  }
  return 0;
}

// > Hello
// > I am parent
// > Hello
// > I am child
// Hello, потому что скопировался буффер printf!
```

=== Форк-бомба

Известный способ убить операционную систему. Суть в том, чтобы без конца создавать процессы.

В файле `/proc/sys/kernel/pid_max` указано максимально возможное количество процессов.

```c
// DO NOT EXECUTE
#include <stdio.h>
#include <unistd.h>

int main() {
  while (1) {
    int pid = fork();
    printf("%d\n", pid);
    if (pid == 0) {
      // child
      while (1) {
      }
    }
  }
  return 0;
}
```

#conf.delimiter

`execve(path, argv[], argp[])` - подменить нашу программу на требуемую. То есть после использования в нек-ой функции `execve` мы больше из неё не вернёмся. Вместо нас будет работать таргет сисколла с тем же _pid_.

`exec(path, argv[])`. Второй вариант. Важно! В конец `argv[]` надо дописать `NULL`.

Можно запускать `execv` в форке, чтобы не потерять контроль.

`wait` дождётся завершения работы какого-нибудь из детей. `waitpid` конкретного ребёнка.

```c
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>

int main() {
  printf("Hello\n");

  int pid = fork();
  if (pid == 0) {
    char* argv[] = {"/usr/bin/ls", "/home/valery/Pictures", NULL};
    int res = execv("/usr/bin/ls", argv);
  }
  int x;
  pid = wait(&x); // разбуди, когда один из детей закончится, верни его статус
  printf("x=%d", x); // x = 0, если в ребёнке был return 0
  return 0;
}
```

== Примечания

- `hd func.o`. Прочитать бинарь.
- `objdump func.o`. Дизассемблировать.
- `ldd`.
