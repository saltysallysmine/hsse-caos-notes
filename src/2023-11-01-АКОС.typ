#import "conf.typ"

#show: doc => conf.conf(
  [2023-11-01-АКОС],
  doc,
)

= АКОС

== Воспоминание

```c
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>

int main() {
  printf("Hello\n");

  int pid = fork();
  if (pid == 0) {  // мы клон
    char* argv[] = {"/usr/bin/ls", "/home/valery/Pictures", NULL};
    int res = execv("/usr/bin/ls", argv);
  }  // иначе оригинал

  int x = 0;
  pid = wait(&x);  // разбуди, когда один из детей закончится, верни его статус
  printf("Child finished with status x=%d",
         x);  // x = 0, если в ребёнке был return 0
  return 0;
}
```

*Процесс* - абстракция ОС. Хранит в себе исполняемый код, выделенную ему память, файловые дескрипторы и атрибуты процесса. Одна программа может соответствовать нескольким процессам.

*Форк* - дубль программы. _Клонируем себя._
*Execv* - подмена себя на таргет. _Убиваем себя, вживляемся в чужое тело._

На прошлом занятии мы смогли запустить свой форк, который позже подменился exec'ом на ls.

*Wait* - уснуть, пока не завершится дочерний процесс.

#quote()[
  _Wait for a child to die.  When one does, put its status in STAT_LOC and return its process ID.  For errors, return (pid_t) -1._
]

#conf.delimiter

*Zombie* - дочерний процесс, который отработал, но на него не был вызван wait. Он висит в системе, ничего не выполняя. Он существует до тех пор, пока его кто-нибудь не дождётся или родитель не закончится.

У exec есть много разных вариантов. Их разница в принимаемых аргументах.

```c
SYNOPSIS
       #include <unistd.h>

       extern char **environ;

       int execl(const char *pathname, const char *arg, ...
                       /*, (char *) NULL */);
       int execlp(const char *file, const char *arg, ...
                       /*, (char *) NULL */);
       int execle(const char *pathname, const char *arg, ...
                       /*, (char *) NULL, char *const envp[] */);
       int execv(const char *pathname, char *const argv[]);
       int execvp(const char *file, char *const argv[]);
       int execvpe(const char *file, char *const argv[], char *const envp[]);
```

- С помощью одного из вариантов wait можно обращаться к конкретному ребёнку.
- Для каждого процесса хранится user_id (uid) и effective_user_id (euid) - айди пользователей, которыми он был запущен. То есть для `sudo a.out: uid = <ваш id>, euid = <sudo>`.
- syscalls seteuid, setuid, ...

== Приостановка процессов

В htop столбец S отвечает за состояние. R - run / runnable, S - stopped (бывает прерываемым и непрерываемым), Z - zombie (defunced - несуществующий). 

*Scheduling* процессов. R не означает, что процесс прямо сейчас отрабатывает на процессоре. Есть очередь процессов на выполнение. Раз в некоторый период ОС приходит к процессору, останавливает его, снимает действующий процесс (возвращая его в очередь) и по сложному алгоритму выбирает следующий процесс на выполнение.

*Состояние Stopped.* Чтобы ввести в него программу, можно использовать оболочку над syscall'ом `nanosleep`, которая называется `sleep`. Можно также в консоли после запуска нажать `^Z` (ctrl-z).

```c
printf("Hello\n");
sleep(5);
printf("Goodbye\n");
```

Во время sleep процесс приостановлен, его состояние S. Дальше его разбудят и пустят на выполнение.

В htop `TIME+` - время, которое процесс успел отработать.

*Будим процесс.* После усыпления процесса, его можно перевести в фоновый режим, то есть разбудить и заставить работать в фоне с помощью `bg`. Также можно вернуть его в нашу консоль с помощью  `fg`.

Список остановленных нами процессов можно увидеть через сложную команду: `ps aux | awk '$8=="T"'` (#link("https://askubuntu.com/questions/897392/list-only-processes-that-are-in-suspended-mode")[StackOverflow]).

== Лимиты

`getrlimit`, `setrlimit`. Можно узнать и задать размер стека и прочее.

`ulimit` - user лимиты.

```bash
$ ulimit -a # узнать установленные ограничения
-t: cpu time (seconds)              unlimited
-f: file size (blocks)              unlimited
-d: data seg size (kbytes)          unlimited
-s: stack size (kbytes)             8192
-c: core file size (blocks)         unlimited
-m: resident set size (kbytes)      unlimited
-u: processes                       61280
-n: file descriptors                1024
-l: locked-in-memory size (kbytes)  8192
-v: address space (kbytes)          unlimited
-x: file locks                      unlimited
-i: pending signals                 61280
-q: bytes in POSIX msg queues       819200
-e: max nice                        0
-r: max rt priority                 0
-N 15: rt cpu time (microseconds)   unlimited
```

== Приоритет

В Linux он называется Niceness. Есть команда nice, позволяющая узнавать и задавать приоритеты процессов. Htop - колонка `NI`.

Чем больше NI, тем более уступчивый процесс. Например, отрицательный NI означает совсем не уступчивый процесс. Default NI = 0.

Как правило, только рут может делать NI довольно маленьким.

В графе real-time priority команды ulimit (`ulimit -r`) указан минимальный NI, который вы можете задавать.

Есть также команда renice и syscall nice.

#quote()[
  _The range of the nice value is +19 (low priority) to -20 (high pri‐ ority).  Attempts to set a nice value outside the range are clamped to the range._
]

== Взаимодействие процессов

Есть три основных способа: сигналы, пайпы и разделяемая память.

=== Сигналы

Например, нам нужно остановить процесс. То есть необходимо послать ему *сигнал* остановки. Это действие выполняет, например, `^C`. На сигнал возможны разные реакции.

1. *Term.* `^C = SIGINT` - сигнал. Стандартная реакция на него - Term (terminate). 
2. *Core.* Core dump возникает в ответ, например, на SIGSEGV. Происходит dump -> процесс сохраняет свой слепок в файл core-dumped, чтобы потом его можно было почитать, например, дебагером.
3. *Ign.*
4. *Cont.*
5. *Stop.*

Список сигналов можно посмотреть в `man 7 signal`.

Чтобы послать сигнал процессу, можно использовать команду `kill`. Через `-<id>` можно указать id сигнала.

- `kill -9` - жёстко убить процесс, не давая ему возможность что-либо сказать на прощание.
- `kill -11` - вызвать segfault у процесса.

Конечно, раздавать сигналы направо и налево может только рут. Иначе вы посылаете сигналы только тому, кого запускали сами.

*Посылаем сигнал себе.* `man 3 raise`. Просим ОС послать сигнал себе. Например, можно нештатно завершать нашу программу, посылая себе abort.

*Перехватываем сигналы.* Устаревший, но рабочий способ. Регистрируем кастомный хэндлер, который вызывается при вызове определённого сигнала.

```cpp
#include <signal.h>
#include <stdio.h>

void sigsegv_handler(int signal) { printf("Caught signal %d\n", signal); }

int main() {
  signal(SIGSEGV, sigsegv_handler);
  signal(SIGINT, sigsegv_handler);

  int* ptr = NULL;
  *ptr = 10; // вызываем segfault
}
```

Программа будет бесконечно вызывать sigsegv, потому что начнёт выполнять инструкцию `*ptr = 10`, упадёт, вызовет handler, затем попробует продолжить инструкцию, упадёт снова и т.д.

Перехватить нельзя только SIGSTOP и SIGKILL (9).


== Примечания

- _Вероятно_, ребёнка ребёнка нельзя дождаться.
- `std::cerr` - вывод в поток ошибок.
- syscalls getcwd, setcwd, chdir.
- signalfd, signalfd4 в man 2.
- `^\` - _вероятно_, жёстко убить процесс.
