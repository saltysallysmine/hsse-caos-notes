#import "conf.typ"

#show: doc => conf.conf(
  [2023-11-09-АКОС],
  doc,
)

= Обработка сигналов, sigaction. Пайпы (pipe). Именованные каналы (fifo, mknod). Разделяемая память (shmget).

=== Воспоминание

Список возможных сигналов лежит в `man 7 signal`.

"Хочу, чтобы когда моя программма падает, она делала определённые нестандартные дейстия". Для этого перехватим сигнал:

```c
#include <signal.h>
#include <stdio.h>

void sigsegv_handler(int signal) { printf("Caught signal %d\n", signal); }

int main() {
  signal(SIGSEGV, sigsegv_handler);
  signal(SIGINT, sigsegv_handler);

  int* ptr = NULL;
  *ptr = 10; // вызываем segfault
}
```

#conf.warning(
  text[
    Это довольно плохой код: в нём небезопасный обработчик. На каком стеке он работает? Что случится, если сигнал придёт, когда хендлер уже работает?
  ]
)

По-хорошему, хендлеру нужен свой стек.

#conf.delimiter

Попробуем вызвать два исключения. Осторожно, этот процесс нужно будет после убить через `kill` :)

```c
void sigsegv_handler(int signal) {
  printf("Caught signal %d\n", signal);
  sleep(10);
  printf("Wake up!\n");
}

int main() {
  signal(SIGINT, sigsegv_handler);   // делаем C-C
  signal(SIGQUIT, sigsegv_handler);  // делаем C-S

  while (1) {
  }
}
```

```
$ ./a.out
^CCaught signal 2
^\Caught signal 3
^C^C^C^C^C^C^C^C^\^\^\^\^\^\^\^\^\^\^CWake up!
Caught signal 3
^Z
```

То есть второй сигнал будет обработан, даже если сейчас обрабатывается первый. Это чревато проблемами: `printf` не потокобезопасный из-за буффера. *Есть целый список функций, которые нельзя вызывать в обработчиках сигнала.*

Решение: *sigaction*. Можно устанавливать правила обработки сигналов.

=== Pipes

*Осуществляет взаимодействие между процессами.* Pipe - труба. Канал связи (\~очередь сообщения), ему соответствует два файловых дескриптора. С одной стороны можно писать, с другой - читать.

Таким образом можно наладить связь между произвольными процессами.

- `man 2 pipe`.

```cpp
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

int main(int argc, char* argv[]) {  // ./a.out somestring
  char buf[1000];
  int pipefd[2];  // [0] - read, [1] - write
  pipe(pipefd);

  int cpid = fork();
  // after fork child and parent have both ends!
  if (cpid == 0) {     // child reads from pipe
    close(pipefd[1]);  // close unused write end

    while (read(pipefd[0], &buf, 1) > 0) {
      write(STDOUT_FILENO, &buf, 1);
    }

    write(STDOUT_FILENO, "\n", 1);
    close(pipefd[0]);
    _exit(0);

  } else {             // parent writes argv[1] to pipe
    close(pipefd[0]);  // close unused read end
    write(pipefd[1], argv[1], strlen(argv[1]));
    close(pipefd[1]);  // reader will see EOF
    wait(NULL);        // wait for child
    exit(0);
  }
}
```

Что будет, если мы будем читать из пайпа, в который никто не пишет? Мы зависнем, будем ждать, пока кто-то напишет. А если писать в тот, из которого никто не читатет? Нам прилетит SIGPIPE.

==== Именованные пайпы (FIFO)

*Особый виртуальный файл, поддерживается ОС как труба.* First In First Out.

- `mkfifo` для создания.
- У такого файла тип `p` - pipe.

*Разница с обычным файлом.*

+ Виртуальность. Запись в обычный файл требует запись на ЖД, а это долго! FIFO намного быстрее.
+ Нельзя перечитать данные, всё читается один раз.

Syscall `mknod` - создать файл общения с устройством или другим процессом. Дальше вы делаете самые обычные read и write.

- `man 3 mkfifo` - обёртка над `mknod`.
- `|` - pipe. В bash `|`, `>`, `>>` (append), `&&`, `||`. Как они реализованы?

=== shm. Shared memory

Пайпы дают однонаправленный доступ. *shm позволяет конкурентный доступ (random access).*

- `shmget`, `shmat`.
- `cd /dev/shm`.

== Примечания

- raise - бросить исключение самим себе.
- pause - заснуть, пока нам не придёт какой-нибудь сигнал.
- *SIGHUP* (код 1). Handup detected. Этот сигнал получают приложения на удалённом сервере, которые вы запустили через ssh, когда обрывается связь. См. утилита nohup. Нужна для того, чтобы приложения продолжали работать даже после того, как вы потеряли связь.
- Другой способ взаимодействия между процессами - создать разделяемую память (map-shared).
- Socket - по сути такой же пайп, но для общения по сети с другим устройством. `bind` - bind a name to a socket.
- syscall `dup` - duplicate fd.

