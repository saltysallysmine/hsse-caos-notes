#import "conf.typ"

#show: doc => conf.conf(
  [2023-11-15-АКОС],
  doc,
)

= АКОС

== Треды

Сначала обсудим, как это работает в C++, потом в C, потом на уровне syscall'ов.

Threads \~ нити, потоки. Лучше просто говорить треды.

*Треды* - это будто процессы, у которых почти все данные общие с исходным. Подробнее: динамическая и статическая памяти у тредов разделяемые, стек у каждого треда свой.

=== C++

```cpp
#include <iostream>
#include <thread>

void f() {
  for (int i = 0; i < 1000; ++i) {
    std::cout << i << '\n';
  }
}

int main() {
  std::thread t(&f);  // можно также передать лямбду
  for (int i = 1000; i < 2000; ++i) {
    std::cout << i << '\n';
  }

  t.join(); // аналог wait for child
}
```

При запуске нужно указать флаг `-pthread` как сигнал использования тредов.

```bash
g++ -pthread threads.cpp && ./a.out
```

При запуске этой программы даже с меньшими числами выводы двух программ могут смешиваться (например, выведется часть числа из треда, потом часть из мейна, потом прочая часть из треда). Это происходит из-за конкурентного доступа к cout. _Помните, что cin и cout потоконебезопасные._

Примеры использования.

+ Умножение матриц. Каждый тред отвечает за свои строку и столбец матриц A и B, но читать они будут из общей памяти.
+ QuickSort. Тредами считать половины массива.

#conf.note(
  text[
    Если не вызвать `join`, будет "terminate called without an active exception. Aborted (core dumped)". Функцию `std::terminate` вызывает деструктор треда, потому что он видит, что не произошло join'а
  ]
)

Есть также метод `t.detach()`. Отпускаем в свободное плавание. Это контр-аналог джоина. Он применяется для фоновых процессов, однако, в целом, используется редко.

#conf.delimiter

==== Пример с push_back. Появление mutex

```cpp
#include <iostream>
#include <thread>
#include <vector>

int main() {
  std::vector<int> v;
  auto f = [&v]() {
    for (int i = 0; i < 100'000; ++i) {
      v.push_back(i);
      std::cout << v.size() << ' ';
    }
  };

  std::thread t1(f);
  std::thread t2(f);

  t1.join();
  t2.join();
}
```

_Segfault_, потому что push_back - нетривиальная операция, в ней делается аллоцирование памяти. Конкурентный доступ к этой операции опасен.

Нужно уметь делать блокировку, то есть запрещать всем другим тредам взаимодействовать с вектором, пока конкретный тред делает push_back. Простейший способ это сделать - mutex.

==== Mutual exclusion

Лежит в `#include <mutex>`. "Занять" блок - `m.lock()`, "освободить" - `m.unlock()`. Если вы занимаете то, что уже занято, вы зависаете в ожидании того, что место для вас освободится.

```cpp
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

int main() {
  std::vector<int> v;
  std::mutex m;

  auto f = [&v, &m]() {
    for (int i = 0; i < 100'000; ++i) {
      m.lock();
      v.push_back(i);
      std::cout << v.size() << ' '; // (*)
      m.unlock();
    }
  };

  std::thread t1(f);
  std::thread t2(f);

  t1.join();
  t2.join();
}
```

Если поменять строку `(*)` со следующей, снова будет конкурентный доступ к cout.

*Dead lock.* Пытаемся лочить несколько мьютексов.

```cpp
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

std::vector<int> v;
std::mutex m1;
std::mutex m2;

void f1() {
  for (int i = 0; i < 10'000; ++i) {
    m1.lock();
    m2.lock();
    v.push_back(i);
    std::cout << i << " from f1" << std::endl;
    m2.unlock();
    m1.unlock();
  }
}

void f2() {
  for (int i = 0; i < 10'000; ++i) {
    m2.lock();
    m1.lock();
    v.push_back(i);
    std::cout << i << " from f2" << std::endl;
    m1.unlock();
    m2.unlock();
  }
}

int main() {
  std::thread t1(f1);
  std::thread t2(f2);

  t1.join();
  t2.join();
}
```

Немного поработает и зависнет из-за того, что не сможет лочить объекты. Ошибка возникает из-за того, что unlock'и делаются в обратном порядке.

_Решение:_ `std::lock`. Позволяет захватить несколько мьютексов.

```cpp
void f1() {
  for (int i = 0; i < 10'000; ++i) {
    std::lock(m1, m2);
    v.push_back(i);
    std::cout << i << " from f1" << std::endl;
    m2.unlock();
    m1.unlock();
  }
}
```

_Вероятно_, строка `std::lock(m1, m2);` решит проблему, даже если добавить её только в f1.

#conf.warning(
  text[
    Проблема этого кода в том, что unlock может не вызваться, если в середине функции выскочит exception. Поэтому lock не советуют использовать в чистом виде.
  ]
)

Вместо `lock` на один мьютекс используйте `std::lock_guard<std::mutex>`.

- `std::lock_guard<std::mutex> lg(m1)`.

Если нужно лочить несколько мьютексов, используйте `std::scoped_lock`.

- `std::scoped_lock<std::mutex, std::mutex> sl1(m1, m2)`.

=== C

Лежит в `#include <pthread.h>`. `pthread_create` для создания. Аналогично есть все функции, которыми мы пользовались выше.

В примере на лекции мы вывели pid из треда и мейна. Они оказались одинаковыми. То есть адресное пространство, не считая стека, и pid у параллельных тредов одинаковое. Как же отличить треды друг от друга?

`gettid` - get thread identification. Почему-то выскакивает WARNING, см. `getgid`.

В setup htop можно добавить колонку TGID (thread group id). Рассмотрим zoom. У него много процессов, у которых одинаковый TGID, но разные PID.

То, что в htop называется PID, по факту является TID.

Мы умеем посылать сигнал процессу, т.е. всей тред-группе. Можем ли мы послать сигнал треду? Да, `tgkill`.

=== Syscalls

Запустим c-программу в strace. Увидим там syscall `clone`. Довольно интересный strace, рассмотрите, как сначала идёт mmap для выделения стека, потом mprotect на чуть меньший размер, чтобы была некая заглушка на вершине стека, при доступе к которой появляется segfault. Потом на этой памяти мы строим стек для clone.

clone - create a child process. Довольно сложный в синтаксисе сискол. Есть аргумент `CLONE_THREAD` (используется вместо устаревшего `CLONE_PID`).

А как реализованы мьютексы? Сискол `futex` - fast user-space locking. Одна из его проблем: функции *futex* в C у вас не будет. Чтобы его вызвать придётся запускать функцию `syscall`. Пример использования описан в `man futex`.

== Примечания

- erai (new, delete) как аналог lock+unlock?
