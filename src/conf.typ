#let conf(title, doc) = {
  set page(
    header: align(
      right,
      title
    ),
    numbering: "— 1 —"
  )

  set text(size: 12pt)

  set par(justify: true)

  show link: underline

  doc
}

#let delimiter = rect(
  width: 100%,
  height: 1pt,
  inset: 4pt,
  fill: black,
)

#let remark(color, header, content) = align(center)[
  #block(width: 90%, inset: 8pt, radius: 4pt)[
    #text(color)[#header.]
    #content
  ]
]


#let note(content) = remark(blue, "NOTE", content)
#let warning(content) = remark(orange, "WARNING", content)
#let error(content) = remark(red, "ERROR", content)
#let todo(content) = remark(red, "TODO", content)
